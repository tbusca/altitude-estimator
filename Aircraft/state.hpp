//
// Created by Trent on 7/22/2022.
//

#ifndef ALTITUDEESTIMATOR_STATE_HPP
#define ALTITUDEESTIMATOR_STATE_HPP
#include <Eigen/Dense>

class State {
public:
    //Current time (s)
    double time {0};

    /* Vector where the first index represents the altitude of the aircraft above sea level (m)
     * and the second index represents the height of the terrain above sea level (m) */
    Eigen::Matrix<double, 2, 1> heightState = Eigen::Matrix<double, 2, 1>::Zero();

    /* Covariance of the state (m^2) */
    Eigen::Matrix<double, 2, 2> covariance = Eigen::Matrix<double, 2 , 2>::Zero();

    /**
     * State constructor
     * @param time Time of state
     * @param state State vector containing the height of the aircraft and terrain above sea level
     * @param covariance sensor noise matrix
     * @return State object
     */
    State(double & time, Eigen::Matrix<double, 2, 1> & state, Eigen::Matrix<double, 2, 2> & covariance) {
        this->time = time;
        this->heightState = std::move(state);
        this->covariance = std::move(covariance);
    };

    /* Default zero arg constructor */
    State() = default;

    /* Default destructor */
    ~State() = default;

};


#endif //ALTITUDEESTIMATOR_STATE_HPP
