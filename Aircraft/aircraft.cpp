//
// Created by Trent on 7/22/2022.
//

#include <aircraft.hpp>

void Aircraft::updateCurrentState(std::shared_ptr<State> newState) {
    /* First move currentState to state history */
    this->stateHistory.emplace_back(std::move(this->currentState));

    this->currentState = std::move(newState);
}

double Aircraft::getHeight() {
    return (this->currentState->heightState(0)-this->currentState->heightState(1));
}
