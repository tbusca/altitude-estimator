#include <gtest/gtest.h>
#include <aircraft.hpp>

TEST(aircraft, state) {
    /* Default constructor test */
    State zeroArgState{};

    EXPECT_EQ(zeroArgState.time, 0);
    EXPECT_EQ(zeroArgState.heightState(0), 0);
    EXPECT_EQ(zeroArgState.heightState(1), 0);
    EXPECT_EQ(zeroArgState.covariance(0,0), 0);
    EXPECT_EQ(zeroArgState.covariance(1,0), 0);
    EXPECT_EQ(zeroArgState.covariance(0,1), 0);
    EXPECT_EQ(zeroArgState.covariance(1,1), 0);


    /* constructor test */
    Eigen::Matrix<double, 2, 1> heightState;
    heightState << 5.5, 1.5;
    double time {3.5};

    Eigen::Matrix<double, 2, 2> covariance = Eigen::Matrix<double, 2, 2>::Zero();
    covariance << 1, 2,
                  3, 4;

    State twoArgState{time, heightState, covariance};

    EXPECT_EQ(twoArgState.time, 3.5);
    EXPECT_EQ(twoArgState.heightState(0), 5.5);
    EXPECT_EQ(twoArgState.heightState(1), 1.5);
    EXPECT_EQ(twoArgState.covariance(0,0), 1);
    EXPECT_EQ(twoArgState.covariance(1,0), 3);
    EXPECT_EQ(twoArgState.covariance(0,1), 2);
    EXPECT_EQ(twoArgState.covariance(1,1), 4);
}

TEST(aircraft, updateCurrentState) {
    Eigen::Matrix<double, 2, 1> initialHeight {0, 0};
    double initialTime = 0;
    Eigen::Matrix<double, 2, 2> covariance = Eigen::Matrix<double, 2, 2>::Zero();


    std::shared_ptr<State> initialState = std::make_shared<State>(initialTime, initialHeight, covariance);

    Eigen::Matrix<double, 2, 1> nextHeight {1, 1};
    double nextTime = 1;
    std::shared_ptr<State> nextState = std::make_shared<State>(nextTime, nextHeight, covariance);

    Aircraft aircraft {};
    aircraft.currentState = initialState;
    aircraft.updateCurrentState(nextState);

    EXPECT_EQ(aircraft.currentState->heightState(0), 1);
    EXPECT_EQ(aircraft.currentState->heightState(1), 1);
    EXPECT_EQ(aircraft.currentState->time, 1);

    EXPECT_EQ(aircraft.stateHistory.size(), 1);
    EXPECT_EQ(aircraft.stateHistory.at(0)->heightState(0),0);
    EXPECT_EQ(aircraft.stateHistory.at(0)->heightState(1),0);
    EXPECT_EQ(aircraft.stateHistory.at(0)->time,0);


};