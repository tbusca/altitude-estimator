//
// Created by Trent on 7/22/2022.
//

#ifndef ALTITUDEESTIMATOR_AIRCRAFT_HPP
#define ALTITUDEESTIMATOR_AIRCRAFT_HPP

#include <state.hpp>
#include <memory>
//#include <measurement.hpp>
#include <naive-system-model.hpp>
class Aircraft {
public:
    /* ID/Name of the aircraft */
    std::string aircraftId {"default"};

    /* state of the aircraft at current time step */
    std::shared_ptr<State> currentState {};

    /* state history of the aircraft -> contains all past states */
    std::vector<std::shared_ptr<State>> stateHistory {};

    /* Updates current state and moves to state history */
    void updateCurrentState(std::shared_ptr<State> newState);

    /* Returns height of aircraft from terrain (m) */
    double getHeight();

    /* System model of aircraft */
    std::shared_ptr<NaiveSystemModel> systemModel {};

    /* Default constructor */
    Aircraft() = default;

    /* Default destructor */
    ~Aircraft() = default;
};


#endif //ALTITUDEESTIMATOR_AIRCRAFT_HPP
