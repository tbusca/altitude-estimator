//
// Created by Trent on 7/24/2022.
//

#ifndef ALTITUDEESTIMATOR_NAIVE_SYSTEM_MODEL_HPP
#define ALTITUDEESTIMATOR_NAIVE_SYSTEM_MODEL_HPP

#include <Eigen/Dense>
#include <utility>
class NaiveSystemModel {
public:
    /* Uncertainty of system model (m^2) */
    Eigen::Matrix<double, 2, 2> systemModelVariance = Eigen::Matrix<double, 2, 2>::Zero();

    /* Returns State Transition Matrix, which steps the prediction forward in time */
    static Eigen::Matrix<double, 2, 2> getStateTransitionMatrix() {

        Eigen::Matrix<double, 2, 2> stateTransitionMatrix = Eigen::Matrix<double, 2, 2>::Zero();
        stateTransitionMatrix << 1, 0, 0, 1;
        return stateTransitionMatrix;
    }

    /**
     * State constructor
     * @param systemModelVariance System model uncertainty matrix
     * @return NaiveSystemModel object
     */
    explicit NaiveSystemModel(const Eigen::Matrix<double, 2, 2> & systemModelVariance) {
        this->systemModelVariance = systemModelVariance;
    }

    NaiveSystemModel() = default;

    ~NaiveSystemModel() = default;
};


#endif //ALTITUDEESTIMATOR_NAIVE_SYSTEM_MODEL_HPP
