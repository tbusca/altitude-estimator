//
// Created by Trent on 7/23/2022.
//

#include <data-handler.hpp>

void DataHandler::processMeasurementSet(const std::vector<std::shared_ptr<Measurement>>& measurementSet) {
    for (auto & measurement : measurementSet) {
        /* Corrects current state with every measurement at current time. KF correct states are independent of order. */

        std::shared_ptr<State> correctState = KalmanFilter::correct(measurement, true);
        measurement->aircraft->updateCurrentState(correctState);
    }
}

void DataHandler::updateTimeStep(double deltaT) {
    for (auto & aircraft : aircraftList) {
        std::shared_ptr<State> updatedState = KalmanFilter::predict(aircraft->currentState, aircraft->systemModel, deltaT);
        aircraft->updateCurrentState(updatedState);
    }
    currentTime+=deltaT;

}

DataHandler::DataHandler(std::vector<std::shared_ptr<Aircraft>> aircraftList, double startTime) {
    this->aircraftList = std::move(aircraftList);
    this->currentTime = startTime;
}


void DataHandler::newInput(const std::vector<std::shared_ptr<Measurement>> &measurementSet) {
    updateTimeStep(abs(measurementSet.at(0)->time-currentTime));
    processMeasurementSet(measurementSet);
    for (auto & aircraft : aircraftList) {
        outputStates.emplace_back(aircraft->currentState);
    }
}
