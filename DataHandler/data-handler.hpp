//
// Created by Trent on 7/23/2022.
//

#ifndef ALTITUDEESTIMATOR_DATA_HANDLER_HPP
#define ALTITUDEESTIMATOR_DATA_HANDLER_HPP

#include <vector>
#include <aircraft.hpp>
#include <map>
#include <kalman-filter.hpp>
#include <queue>
class DataHandler {
public:
    /* List of aircraft tracked by dataHandler */
    std::vector<std::shared_ptr<Aircraft>> aircraftList {};

    /* Current time step of dataHandler */
    double currentTime {0};

    /**
    * Calls the KF Predict for each aircraft
    * @param aircraftList set of measurements each at the same time
    */
    DataHandler(std::vector<std::shared_ptr<Aircraft>> aircraftList, double startTime);

    /**
    * Calls the KF Correct for each measurement/aircraft within the set
    * @param measurementSet set of measurements each at the same time
    */
    static void processMeasurementSet(const std::vector<std::shared_ptr<Measurement>>& measurementSet);

    /**
    * Handels data flow when new input of measurements is received
    * @param measurementSet new measurement set
    */
    void newInput(const std::vector<std::shared_ptr<Measurement>>& measurementSet);

    /* states output at the end of each correct step */
    std::vector<std::shared_ptr<State>> outputStates {};

    void updateTimeStep(double deltaT);
};


#endif //ALTITUDEESTIMATOR_DATA_HANDLER_HPP
