#include <gtest/gtest.h>
#include <data-handler.hpp>
#include <fstream>
TEST(DataHandlerTest, integrationTest) {

    double initialTime = 0;
    Eigen::Matrix<double, 2, 1> initialHeightState(217.36901885457337, 151.826759655);
    Eigen::Matrix<double, 2, 2> initialCovariance = Eigen::Matrix2d::Zero();
    initialCovariance << 1E-5, 0,
                          0, 1E-1;

    /* Create Aircraft, initial state, and system model */
    std::shared_ptr<State> state = std::make_shared<State>(initialTime, initialHeightState, initialCovariance);
    std::shared_ptr<Aircraft> aircraft = std::make_shared<Aircraft>();
    aircraft->currentState = state;

    Eigen::Matrix<double, 2, 2> systemModelVariance = Eigen::Matrix2d::Zero();
    systemModelVariance << 1E-3, 0,
                           0, 1E-1;
    std::shared_ptr<NaiveSystemModel> systemModel = std::make_shared<NaiveSystemModel>(systemModelVariance);
    aircraft->systemModel = systemModel;
    std::vector<std::shared_ptr<Aircraft>> aircraftList {aircraft};


    /* Create sensor models */
    Eigen::Matrix<double, 2, 2> sensorNoiseGps = Eigen::Matrix2d::Zero();
    sensorNoiseGps << 1E-15, 0,
                      0, 1E-15;
    std::shared_ptr<GpsModel> gps = std::make_shared<GpsModel>("gps", sensorNoiseGps);

    Eigen::Matrix<double, 2, 2> sensorNoiseAlt1 = Eigen::Matrix2d::Zero();
    sensorNoiseAlt1 << 1E-6, 0,
                       0, 1E-6;
    std::shared_ptr<AltimeterModel> alt1 = std::make_shared<AltimeterModel>("alt1", sensorNoiseAlt1);

    Eigen::Matrix<double, 2, 2> sensorNoiseAlt2 = Eigen::Matrix2d::Zero();
    sensorNoiseAlt2 << 1E-6, 0,
                       0, 1E-6;
    std::shared_ptr<AltimeterModel> alt2 = std::make_shared<AltimeterModel>("alt2", sensorNoiseAlt2);

    DataHandler handler = {aircraftList, initialTime};

    double time = 0;
    int index{};
    double inGps, inAlt1, inAlt2;

    std::ifstream file("./log2.csv");
    std::string line;
    char c;
    while (file >> index >> c >> inGps >> c >> inAlt1 >> c >> inAlt2) {
        if (file.eof()) {
            break;
        }
        time += 0.01;

        Eigen::Matrix<double, 2, 1> inGpsHeightState{inGps, 0};
        std::shared_ptr<Measurement> gpsMeasurement = std::make_shared<Measurement>(time, inGpsHeightState,
                                                                                    gps, aircraft);

        Eigen::Matrix<double, 2, 1> inAlt1HeightState{0, inAlt1};
        std::shared_ptr<Measurement> alt1Measurement = std::make_shared<Measurement>(time, inAlt1HeightState,
                                                                                     alt1, aircraft);
        Eigen::Matrix<double, 2, 1> inAlt2HeightState{0, inAlt2};
        std::shared_ptr<Measurement> alt2Measurement = std::make_shared<Measurement>(time, inAlt2HeightState,
                                                                                     alt2, aircraft);

        std::vector<std::shared_ptr<Measurement>> measurementVec{gpsMeasurement, alt1Measurement, alt2Measurement};
        handler.newInput(measurementVec);
    }

    std::ofstream outFile;
    outFile.open("./out2.csv");

    for (auto & output : handler.outputStates) {
        outFile << std::setprecision(10) << "\n"<<output->time*100<<","<<output->heightState(0)<<","<<output->heightState(1)<<","<<output->heightState(0)-output->heightState(1);
    }
     outFile.close();
}