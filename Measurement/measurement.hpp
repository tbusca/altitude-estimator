//
// Created by Trent on 7/22/2022.
//

#ifndef ALTITUDEESTIMATOR_MEASUREMENT_HPP
#define ALTITUDEESTIMATOR_MEASUREMENT_HPP

#include <sensor-model.hpp>
#include <utility>
#include <aircraft.hpp>
struct Measurement {
    /* Time the measurement was recorded (s) */
    double time {0};

    /* Measurement from sensor */
    Eigen::Matrix<double, 2, 1> measurement = Eigen::Vector2d::Zero();

    /* Sensor from which the measurement originated */
    std::shared_ptr<iSensorModel> sensor {};

    /* Aircraft which the measurement originated */
    std::shared_ptr<Aircraft> aircraft {};


    Measurement() = default;
    ~Measurement() = default;

    /**
     * Measurement constructor
     * @param time Time measurement was recorded
     * @param measurement Measurement from sensor
     * @param sensor Sensor from which the measurement originated
     * @param aircraft Aircraft which the measurement originated
     * @return measurement object
     */
    Measurement(double time, Eigen::Matrix<double, 2, 1> measurement, std::shared_ptr<iSensorModel> sensor, std::shared_ptr<Aircraft> aircraft) {
        this->time = time;
        this->measurement = std::move(measurement);
        this->sensor = std::move(sensor);
        this->aircraft = std::move(aircraft);
    }
};


#endif //ALTITUDEESTIMATOR_MEASUREMENT_HPP
