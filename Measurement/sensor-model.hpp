//
// Created by Trent on 7/25/2022.
//

#ifndef ALTITUDEESTIMATOR_SENSOR_MODEL_H
#define ALTITUDEESTIMATOR_SENSOR_MODEL_H

#include <Eigen/Dense>
class iSensorModel {
public:

    /* Unique ID of the sensor */
    std::string id {"none"};

    /* Measurement Uncertainty (m^2) */
    Eigen::Matrix<double, 2, 2> sensorNoise = Eigen::Matrix<double, 2, 2>::Zero();

    /* Returns measurement matrix (converts state to measurement frame) */
    virtual Eigen::Matrix<double, 2, 2> getMeasurementMatrix() = 0;

    iSensorModel() = default;

    ~iSensorModel() = default;

    /**
     * interface sensorModel constructor
     * @param id unique id of the sensor
       @param sensorNoise sensor noise matrix
     * @return interface sensor model object
     */
    iSensorModel(const std::string & id, const Eigen::Matrix<double, 2, 2> & sensorNoise) {
        this->id = id;
        this->sensorNoise = sensorNoise;
    }

};

class GpsModel: public iSensorModel {
public:

    /* Returns measurement matrix (converts state to measurement frame) */
    Eigen::Matrix<double, 2, 2> getMeasurementMatrix() override {
        Eigen::Matrix<double, 2, 2> measurementMatrix = Eigen::Matrix<double, 2, 2>::Zero();
        measurementMatrix << 1, 0,
                             0, 0;
        return measurementMatrix;
    }

    /**
     * GpsModel constructor
     * @param id unique id of the sensor
       @param sensorNoise sensor noise matrix
     * @return GPS model object
     */
    GpsModel(const std::string& id, const Eigen::Matrix<double, 2, 2>& sensorNoise)
    :iSensorModel(id, sensorNoise) {}

};

class AltimeterModel: public iSensorModel {
public:

    /* Returns measurement matrix (converts state to measurement frame) */
    Eigen::Matrix<double, 2, 2> getMeasurementMatrix() override {
        Eigen::Matrix<double, 2, 2> measurementMatrix = Eigen::Matrix<double, 2, 2>::Zero();
        measurementMatrix << 0, 0,
                             1, -1;
        return measurementMatrix;
    }

    /**
    * AltimeterModel constructor
    * @param id unique id of the sensor
      @param sensorNoise sensor noise matrix
    * @return Altimeter model object
    */
    AltimeterModel(const std::string& id, const Eigen::Matrix<double, 2, 2>& sensorNoise)
    :iSensorModel(id, sensorNoise) {}

};


#endif //ALTITUDEESTIMATOR_SENSOR_MODEL_H
