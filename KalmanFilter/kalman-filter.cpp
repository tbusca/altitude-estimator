#include <kalman-filter.hpp>
std::shared_ptr<State> KalmanFilter::predict(const std::shared_ptr<State> & currentState, const std::shared_ptr<NaiveSystemModel> & systemModel, double deltaT) {

    Eigen::Matrix<double, 2, 2> stateTransitionMatrix = systemModel->getStateTransitionMatrix();

    /* Step 1. Step state forward */
    Eigen::Matrix<double, 2, 1> newHeightState = stateTransitionMatrix * currentState->heightState;

    /* Step 2. Step covariance forward */
    Eigen::Matrix<double, 2, 2> newCovariance = (stateTransitionMatrix * currentState->covariance * stateTransitionMatrix.transpose()) + systemModel->systemModelVariance;

    double newTime = currentState->time + deltaT;

    return std::make_shared<State>(newTime, newHeightState, newCovariance);
};

std::shared_ptr<State> KalmanFilter::correct(const std::shared_ptr<Measurement> & measurement, bool useMahalanobisGate) {

    /* Step 1: Calculate Kalman Gain reference https://www.kalmanfilter.net/multiSummary.html for notation */
    Eigen::Matrix<double, 2, 2> H = measurement->sensor->getMeasurementMatrix();
    Eigen::Matrix<double, 2, 2> R = measurement->sensor->sensorNoise;
    Eigen::Matrix<double, 2, 2> P = measurement->aircraft->currentState->covariance;

    /* S = innovation covariance (covariance projected into measurement frame + R) */
    Eigen::Matrix<double, 2, 2> S_inv = ((H*P*H.transpose())+R).inverse();
    Eigen::Matrix<double, 2, 2> K = P*H.transpose()*S_inv;

    /* Step 2: Update estimate with measurement  */
    Eigen::Matrix<double, 2, 1> residual = measurement->measurement - (H*measurement->aircraft->currentState->heightState);
    Eigen::Matrix<double, 2, 1> newHeightState = measurement->aircraft->currentState->heightState + K*residual;

    double mahalanobisSquared = residual.transpose()*S_inv*residual;
    /* If the distribution is Gaussian M^2 is defined by a Chi^2 distribution.
     * Inverse chi square taken to gate out measurements which fall outside 3 sigma */
    double mahalanobisGate = 11.82916;

    /* Step 3: Update covariance with measurement */
    Eigen::Matrix<double, 2, 2> I = Eigen::Matrix<double, 2, 2>::Identity();
    Eigen::Matrix<double, 2, 2> newCovariance = ((I-K*H)*P*(I-K*H).transpose())+(K*R*K.transpose());

    /* Time has already been updated in predict step */
    std::shared_ptr<State> newState = (useMahalanobisGate && mahalanobisSquared > mahalanobisGate)
            ? std::make_shared<State>(measurement->aircraft->currentState->time, measurement->aircraft->currentState->heightState, measurement->aircraft->currentState->covariance)
            : std::make_shared<State>(measurement->aircraft->currentState->time, newHeightState, newCovariance);

    return newState;
};