//
// Created by Trent on 7/23/2022.
//

#ifndef ALTITUDEESTIMATOR_KALMAN_FILTER_HPP
#define ALTITUDEESTIMATOR_KALMAN_FILTER_HPP

#include <measurement.hpp>
#include <iostream>
namespace KalmanFilter {

    /**
     * Updates aircraft state based on the system model and elapsed time
     * @param currentState State of aircraft before prediction
     * @param NaiveSystemModel System model of the aircraft
     * @param currentState Difference in time with the current state
     * @return New current State object with updated time and state
     */
    std::shared_ptr<State> predict(const std::shared_ptr<State> & currentState, const std::shared_ptr<NaiveSystemModel> & systemModel, double deltaT);

    /**
     * Updates aircraft state based on new measurement
     * @param measurement State of aircraft before prediction
     * @param malahanobisGate Turns on mahalanobis gate when set to true
     * @return New current State object with updated state
     */
    std::shared_ptr<State> correct(const std::shared_ptr<Measurement> & measurement, bool useMahalanobisGate);
}

#endif //ALTITUDEESTIMATOR_KALMAN_FILTER_HPP
