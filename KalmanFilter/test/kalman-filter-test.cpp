#include <gtest/gtest.h>
#include <kalman-filter.hpp>

TEST(KalmanFilterTest, Predict) {

    double initialTime = 0;
    Eigen::Matrix<double, 2, 1> initialHeightState(0, 1);
    Eigen::Matrix<double, 2, 2> initialCovariance = Eigen::Matrix2d::Zero();

    initialCovariance << 1E-5, 0,
                         0, 1E-5;

    std::shared_ptr<State> initialState = std::make_shared<State>(initialTime, initialHeightState, initialCovariance);

    Eigen::Matrix<double, 2, 2> variance = Eigen::Matrix2d::Zero();
    variance << 1E-5, 0,
                0, 1E-5;
    std::shared_ptr<NaiveSystemModel> systemModel = std::make_shared<NaiveSystemModel>(variance);

    double deltaT = 0.01;
    std::shared_ptr<State> newState = KalmanFilter::predict(initialState, systemModel, deltaT);

    EXPECT_EQ(newState->heightState(0), 0);
    EXPECT_EQ(newState->heightState(1), 1);

    EXPECT_EQ(newState->covariance(0, 0), 2E-5);
    EXPECT_EQ(newState->covariance(0, 1), 0);
    EXPECT_EQ(newState->covariance(1, 0), 0);
    EXPECT_EQ(newState->covariance(1, 1), 2E-5);
};

TEST(KalmanFilterTest, Correct) {
    double initialTime = 1;
    Eigen::Matrix<double, 2, 1> initialHeightState(2, 1);
    Eigen::Matrix<double, 2, 2> initialCovariance = Eigen::Matrix2d::Zero();
    initialCovariance << 1E-10, 0,
                         0, 1E-5;

    std::shared_ptr<State> state = std::make_shared<State>(initialTime, initialHeightState, initialCovariance);

    std::shared_ptr<Aircraft> aircraft = std::make_shared<Aircraft>();
    aircraft->currentState = state;

    Eigen::Matrix<double, 2, 2> sensorNoise = Eigen::Matrix2d::Zero();
    sensorNoise << 1E-10, 0,
                   0, 1E-10;
    std::shared_ptr<GpsModel> gps = std::make_shared<GpsModel>("gps", sensorNoise);
    Eigen::Matrix<double, 2, 1> gpsHeightState {4, 0};

    std::shared_ptr<Measurement> measurement = std::make_shared<Measurement>(initialTime, gpsHeightState, gps, aircraft);
    std::shared_ptr<State> gpsCorrectState = KalmanFilter::correct(measurement, false);
    aircraft->updateCurrentState(gpsCorrectState);

    EXPECT_EQ(aircraft->currentState->heightState(0), 3);
    EXPECT_EQ(aircraft->currentState->heightState(1), 1);
    EXPECT_NEAR(aircraft->currentState->covariance(0,0), 5E-11, 1E-22);
    EXPECT_EQ(aircraft->currentState->covariance(0,1), 0);
    EXPECT_EQ(aircraft->currentState->covariance(1,0), 0);
    EXPECT_EQ(aircraft->currentState->covariance(1,1), 1E-5);
    EXPECT_EQ(aircraft->currentState->time, 1);

}