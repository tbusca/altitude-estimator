add_subdirectory(test)

set(This KalmanFilter)

set(HEADERS kalman-filter.hpp)

set(SOURCES kalman-filter.cpp)

add_library(${This} ${SOURCES} ${HEADERS})
set_target_properties(${This} PROPERTIES LINKER_LANGUAGE CXX)

target_link_libraries(
        ${This}
        Eigen3::Eigen
        Measurement)